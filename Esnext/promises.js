function falarDepoisDe(segundos, frase) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(frase);
    }, segundos * 1000);
  });
}

console.log("Testando sincronismo");
console.log("Testando sincronismo 2");
console.log("Testando sincronismo 3");

falarDepoisDe(3, "Que legal!")
  .then((frase, abc) => frase.concat("?!?"))
  .then((outraFrase) => console.log(outraFrase))
  .catch(e=> console.error(e))
