let a = 1
console.log(a)

let p = new Promise((cumprirPromessa) => {
    cumprirPromessa(3)
})

p.then((valor) => {
    console.log(valor)
})

function primeiraLetra(string){
    return string[0]
}

function primeiraPosicao(array){
    return array[0]
}

const letraMinuscula = letra => letra.toLowerCase()

new Promise(function(cumprirPromessa){
    cumprirPromessa(['Ana', 'Bia','Carlos','Daniel'])
})
.then(primeiraPosicao)
.then(primeiraLetra)
.then(letraMinuscula)
.then(console.log)