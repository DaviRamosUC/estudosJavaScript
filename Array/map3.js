Array.prototype.map2 = function(callback) {
    const suporte =[];
    for (let index = 0; index < this.length; index++) {
        suporte.push(callback(this[index], index, this))
    }
    return suporte
}

const carrinho = [
    '{ "nome": "Borracha", "preco": 3.45} ',
    '{ "nome": "Caderno", "preco": 13.90} ',
    '{ "nome": "Kit de Lapis", "preco": 41.22} ',
    '{ "nome": "Caneta", "preco": 7.50} ',
  ];
  
  //Retornar um array apenas com os preços
  
  const objetificando = (e) => JSON.parse(e);
  const getPreco = (e) => e.preco;
  
  let resultado = carrinho.map2(objetificando).map2(getPreco);
  console.log(resultado);
  