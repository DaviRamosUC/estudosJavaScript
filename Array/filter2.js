Array.prototype.filter2 = function(callback) {
    const suporte = [];
    for (let index = 0; index < this.length; index++) {
        if (callback(this[index], index, this)) {
            suporte.push(this[index]);
        }
    }
    return suporte
}

const produtos = [
    {nome: 'Notebook', preco: 2499, fragil: true},
    {nome: 'Ipad Pro', preco: 4199, fragil: true},
    {nome: 'Copo de Vidro', preco: 12.49, fragil: true},
    {nome: 'Copo de Plástico', preco: 18.99, fragil: false},
];

const produtosCaros = p =>{
   return p.preco>=500
}

const produtosFrageis = p => p.fragil

console.log(produtos.filter2(produtosCaros).filter2(produtosFrageis))