const aprovados = ["Agatha", "Aldo", "Daniel", "Raquel"];

aprovados.forEach(function(nome, indice){
    console.log(`${indice +1 }) ${nome}`)
})
console.log("------------")

aprovados.forEach(function(nome, indice, array){
    console.log(`${indice +1 }) ${nome}`)
    console.log(array)
})
console.log("------------")

aprovados.forEach((nome)=> console.log(`${nome}`))

const exibirAprovados = aprovado => console.log(aprovado)
console.log("------------")
aprovados.forEach(exibirAprovados)