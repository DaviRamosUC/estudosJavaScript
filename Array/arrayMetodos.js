const pilotos = ['Vetter', 'Alonso', 'Raikkonen', 'Massa']
pilotos.pop() //Remove o fim
console.log(pilotos)

pilotos.push('Verstappen')
console.log(pilotos)

pilotos.shift() //Remove do inicio
console.log(pilotos)

pilotos.unshift('Hamilton')
console.log(pilotos)

//splice pode adicionar ou remover elementos

//adicionar
pilotos.splice(2,0, 'Bottas', 'Massa')
console.log(pilotos)

//remover elementos
pilotos.splice(3,1)
console.log(pilotos)

const algunsPilotos1 = pilotos.slice(2) //Novo array
console.log(algunsPilotos1)

const algunsPilotos2 = pilotos.slice(1,4) //Não inclui o indice 4