// pessoa -> 123 -> {...}
const pessoa = { nome: 'João'}
pessoa.nome = 'Pedro'
console.log(pessoa)

//           456 = endereço de memoria
// pessoa -> 456 -> {...} 
//pessoa = { nome: 'Ana'}

Object.freeze(pessoa) // congelando o objeto pessoa e ignora possíveis mudanças

pessoa.nome = 'Maria'
pessoa.endereco = 'Rua ABC'
delete pessoa.nome

console.log(pessoa)
console.log(pessoa.nome)

const pessoaConstante = Object.freeze({nome: 'João'})
pessoaConstante.nome = 'Ana'
console.log(pessoaConstante)