// CÓDIGO NÃO EXECUTÁVEL

//Procedural
processamento(valor1, valor, valor3);

//OO - Objetos
let objeto = {
  valor1,
  valor2,
  valor3,
  processamento() {
    //...
  },
};

objeto.processamento(); //Foco passou a ser o objeto...

//Principios importantes: 
// 1. abstração
// 2. encapsulamento
// 3. herança
// 4. polimorfismo
