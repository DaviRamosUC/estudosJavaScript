// função Factory com argumentos
function criarProduto(nome,preco){
    return{
        nome: nome,
        preco: preco,
        desconto: 0.1
    }
}

console.log(criarProduto('Sabão', 50))