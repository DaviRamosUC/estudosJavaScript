const pessoa = {
    saudacao: 'Bom dia!',
    falar(){
        console.log(this.saudacao)
        // console.log(saudacao) não funciona sem o this == não existe referência
    }
}

pessoa.falar()
const falar = pessoa.falar
falar() //Gera um undefind pelo fato de estar chamando falar e this da constante falar

const falarDePessoa = pessoa.falar.bind(pessoa)
falarDePessoa()