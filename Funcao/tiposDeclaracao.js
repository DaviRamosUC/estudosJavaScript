// carregamento provisório das funções
console.log(soma(3, 4));

// function declaration
function soma(x, y) {
    return x + y;
}

//function expression
const sub = function (x, y) {
    return x - y;
};
// Carregamento postergardo
console.log(sub(3, 4));

//named function expression
const mult = function mult(x, y) {
    return x * y;
};
// Carregamento postergardo
console.log(mult(3, 4));
