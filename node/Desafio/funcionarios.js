const url = "http://files.cod3r.com.br/curso-js/funcionarios.json";

const axios = require("axios");

//retornar o funcionario cujo (Ser mulher, chinesa e ter o menor salário)
axios.get(url).then((response) => {
  const funcionario = response.data;

  const funcionariosMulherChinesa = funcionario
    .filter((pessoa) => {
      return pessoa.genero == "F" && pessoa.pais == "China";
    })
    .reduce((resultado, atual) => {
      if (atual.salario < resultado.salario) {
        resultado = atual;
      }
      return resultado;
    });

  console.log(funcionariosMulherChinesa);
});
