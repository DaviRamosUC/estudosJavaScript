const moduloA = require('../../../Aula1/moduloA') //respeitar o nome do modulo

console.log(moduloA.ola)

const _ = require('lodash') //Não é necessário fazer caminho relativo para importações com node

// setInterval(()=> console.log(_.random(500,600)),2000)

const c = require('./PastaC')//usando padrão index.js
console.log(c.ola2)

const http = require('http')
http.createServer((req, res)=>{
    res.write('Bom dia2')
    res.end()
}).listen(8080)