const func = function (numero, minimo, maximo, inclusivo = false) {
  if (inclusivo) {
      return numero>=minimo && numero<=maximo
  }
  return numero> minimo && numero<maximo
};

console.log(func(50,10,100));
console.log(func(16,100,160));
console.log(func(3,150,3));
console.log(func(3,150,3,true));

