const filtrarNumeros = valor =>{
    return valor.filter(x => typeof x === 'number')
}

console.log(filtrarNumeros(["Javascript", 1, "3", "Web", 20]))
console.log(filtrarNumeros(["a", "c"]))