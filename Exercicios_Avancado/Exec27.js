const inverter = valor =>{
    const obj = {}
    Object.entries(valor).forEach(atual =>{
       obj[atual[1]]=atual[0]
    })
    return obj
}

console.log(inverter({ a: 1, b: 2, c: 3}))