const func = function (valor){
    if (typeof valor == 'boolean') {
        return !valor
    }else if(typeof valor == 'number'){
        return valor * (-1)
    }else{
        return "booleano ou número esperados, mas o parâmetro é do tipo ..."
    }
}

console.log(func(true))
console.log(func("6"))
console.log(func(-2000))
console.log(func("programação"))