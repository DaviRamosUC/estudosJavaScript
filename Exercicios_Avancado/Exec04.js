const mes = function(valor) {
    if (valor<=0 || valor>12) {
       return ("Valor não representa um mês do ano, tente novamente")
    }
    let resultado;
    switch (valor) {
        case 1:
            resultado = 'janeiro'        
            break;
        case 2:
            resultado = 'Fevereiro'        
            break;
        case 3:
            resultado = 'Março'        
            break;
        case 4:
            resultado = 'Abril'        
            break;
        case 5:
            resultado = 'Junho'        
            break;
        case 6:
            resultado = 'Julho'        
            break;
        case 7:
            resultado = 'Agosto'        
            break;
        case 8:
            resultado = 'Setembro'        
            break;
        case 9:
            resultado = 'Outubro'        
            break;
        case 10:
            resultado = 'Maio'        
            break;
        case 11:
            resultado = 'Novembro'        
            break;
        case 12:
            resultado = 'Dezembro'        
            break;        
    }
    return resultado
}

console.log(mes(0))