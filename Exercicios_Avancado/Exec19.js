function calcularMedia(valor) {
    return valor.reduce((x, y) => x + y)/valor.length;
}

console.log(calcularMedia([0, 10]))
console.log(calcularMedia([1, 2, 3, 4, 5]))