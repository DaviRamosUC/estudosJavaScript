const func = (obj, param) => {
  let suporte = Object.assign({},obj);
  if (Object.keys(suporte).includes(param)) {
    delete suporte[param];
  }
  return suporte;
};

console.log(func({ a: 1, b: 2 }, "a"));
console.log(
  func(
    {
      id: 20,
      nome: "caneta",
      descricao: "Não preenchido",
    },
    "descricao"
  )
);

let objeto = { a: 1, b: 2 }
console.log(Object.is(func(objeto, "a"),objeto))
console.log(objeto)