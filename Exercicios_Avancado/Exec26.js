const removerVogais = valor => {
    const vogais = ['a', 'e', 'i', 'o', 'u']

    return valor.split('').reduce((resultado, atual) => {
        if(!vogais.includes(atual)){
            resultado += atual
        }
        return resultado
    },'')
}

console.log(removerVogais("Cod3r"))
console.log(removerVogais("Fundamentos"))