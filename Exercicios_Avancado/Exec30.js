const recerberMelhorEstudante = (obj) => {
  let suporte = Object.entries(obj);
  let resultado = [];
  suporte.forEach((x) => {
    let objetoSup = {};
    let media = x[1].reduce((x, y) => x + y) / x[1].length;
    objetoSup['nome'] = x[0]
    objetoSup['media'] = media
    resultado.push(objetoSup);
  });
  return resultado.sort((x, y) =>y.media>x.media)[0]
};

console.log(
  recerberMelhorEstudante({
    Joao: [8, 7.6, 8.9, 6], // média 7.625
    Mariana: [9, 6.6, 7.9, 8], // média 7.875
    Carla: [7, 7, 8, 9], // média 7.75
  })
);
