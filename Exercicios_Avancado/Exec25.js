const buscarPalavrasSemelhantes = (str, valor) => {
    return valor.filter(x => x.includes(str));
}

console.log(buscarPalavrasSemelhantes("pro", ["programação", "mobile", "profissional"]))

console.log(buscarPalavrasSemelhantes("python", ["javascript", "java", "c++"]))