function bhaskara(ax2, bx, c) {
  let resultado = [];
  function delta() {
    return ((bx * bx) -(4 * ax2 * c))
  }

  if (delta()>0){
      resultado.push((-bx + Math.sqrt(delta())) / (2 * ax2));
      resultado.push((-bx - Math.sqrt(delta())) / (2 * ax2));
      return resultado;
  }else{
      return "Delta negativo"
  }


}

console.log(bhaskara(2, 3, -5));
