const notasAlunos = (nota) => {
  if (nota < 38) {
    console.log("Reprovado: " + nota);
  } else {
    nota = Math.ceil(nota / 5) * 5;
    return console.log("Aprovado: " + nota)
  }
};

notasAlunos(38);