const op = (a, b) => {
  return {
    soma: a + b,
    subtracao: a - b,
    produto: a * b,
    divisao: a / b
  };
};

let resultado = op(10,2);
const {soma, subtracao, produto, divisao} = resultado

console.log(soma, subtracao, produto, divisao);
