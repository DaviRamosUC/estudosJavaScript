function calculaFatorial(valor) {
  //fatorial de 0 é 1
  //fatorial de 1 é 1 = 1 * 1
  //fatorial de 2 é 2 = 2 * 1

  if (valor == 0 || valor == 1) {
    return 1;
  } else if (valor > 1) {
    let resultado = 1;
    let op = valor;
    while (op > 0) {
      resultado = resultado * op;
      op--;
    }
    return resultado;
  }
}

console.log(calculaFatorial(2));
