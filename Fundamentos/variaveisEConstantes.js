//consigo redeclarar 2x
var a //variável utilizada antigamente

a= "tentativa"

console.log(a)

//só pode declarar ela uma vez, porém ela pode mudar o valor
let b //essa variável muda a todo momento

b = 123

console.log(b)

const d = 50//constante declarada o valor logo após definir

console.log(d)