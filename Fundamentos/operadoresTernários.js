const resultado = nota => nota>=7 ? 'Aprovado' : 'Reprovado'
/*
O código de cima poderia ser escrito da seguinte forma:

const resultado2 = nota => {
    return nota>=7 ? 'Aprovado' : 'Reprovado'
}

*/

console.log(resultado(7.1))
console.log(resultado(6.7))