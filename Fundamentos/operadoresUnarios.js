let  num1 = 1
let  num2 = 2

num1++
console.log(num1)
--num1 //tem uma exigência maior para que a operação ocorra do que (num1--)
console.log(num1)
console.log(++num1 === num2--) // da true porque o decremento só rola depois da comparação
console.log(++num1 === --num2) // da false porque a decrementação acontece primeiro (antes de comparar)

