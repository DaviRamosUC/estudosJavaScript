const escola = "Cod3r"

console.log(escola.charAt(4))
console.log(escola.charAt(2))
console.log(escola.charCodeAt(3))//retorna o valor da tabela ASCII

console.log(escola.indexOf('3')) // revela  a posição do char que vc está procurando

console.log(escola.substring(0,3))//mostra somente o periodo da string definida

console.log('escola '.concat(escola).concat("!"))//equivale ao + num System.out.Println() do java

console.log(escola.replace(3,'e'))//altera letras dentro da string

console.log('Ana,Maria,João'.split(',')) // transforma uma lista em um array

console.log('3'+2) // neste caso ele vai concatenar os valores String e number